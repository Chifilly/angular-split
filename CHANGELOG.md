# Change Log

## 1.1.0

### New settings

* These settings allow you to enable and disable specific file types to be included in the split view
  * `angular-split.enable.template` - default: `true`
  * `angular-split.enable.script` - default: `true`
  * `angular-split.enable.style` - default: `true`
  * `angular-split.enable.spec` - default: `false`

* These settings allow you to configure the file extensions to look for for each file type
  * `angular-split.files.template` - default: `["html"]`
  * `angular-split.files.script` - default: `["ts"]`
  * `angular-split.files.style` - default: `["css", "scss", "sass", "less"]`
  * `angular-split.files.spec` - default: `["spec.ts"]`

### Removed settings

* `angular-split.spec` - Replaced with the new `angular-split.enable.spec`

## 1.0.0

* Initial release
