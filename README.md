# Angular Split

[![Visual Studio Code Marketplace](https://img.shields.io/visual-studio-marketplace/azure-devops/installs/total/chifilly.angular-split.svg?style=popout)](https://marketplace.visualstudio.com/items?itemName=chifilly.angular-split)

## Features

An extension that allows you to easily open the corresponding Angular Component files (template, script, stylesheet and spec files) side-by-side with a single key binding.

You can also configure which files you wish to open, as well as the file extensions to look for when attempting to split.

## Extension Settings

| Setting | Description | Default |
| --- | --- | --- |
| `angular-split.enable.template` | Whether template file should be included when split viewing the component files | `true` |
| `angular-split.enable.script` | Whether script file should be included when split viewing the component files | `true` |
| `angular-split.enable.style` | Whether stylesheet file should be included when split viewing the component files | `true` |
| `angular-split.enable.spec` | Whether spec file should be included when split viewing the component files | `false` |
| `angular-split.files.template` | The list of file extensions to try and open for the component template | `["html"]` |
| `angular-split.files.script` | The list of file extensions to try and open for the component script | `["ts"]` |
| `angular-split.files.style` | The list of file extensions to try and open for the component stylesheet | `["css", "scss", "sass", "less"]` |
| `angular-split.files.spec` | The list of file extensions to try and open for the component specification | `["spec.ts"]` |

## Key Bindings

| Command | Description | Binding |
| --- | --- | --- |
| `angular-split.execute` | Execute the split action | `Alt + S` (`Shift + Alt + S` on Mac) |

## Known Issues

None

## Change Log

### 1.1.0

#### New settings

* These settings allow you to enable and disable specific file types to be included in the split view
  * `angular-split.enable.template` - default: `true`
  * `angular-split.enable.script` - default: `true`
  * `angular-split.enable.style` - default: `true`
  * `angular-split.enable.spec` - default: `false`

* These settings allow you to configure the file extensions to look for for each file type
  * `angular-split.files.template` - default: `["html"]`
  * `angular-split.files.script` - default: `["ts"]`
  * `angular-split.files.style` - default: `["css", "scss", "sass", "less"]`
  * `angular-split.files.spec` - default: `["spec.ts"]`

#### Removed settings

* `angular-split.spec` - Replaced with the new `angular-split.enable.spec`

### 1.0.0

* Initial release
